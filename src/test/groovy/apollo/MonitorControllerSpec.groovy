package apollo

import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class MonitorControllerSpec extends Specification {

  @Inject
  @Client("/")
  RxHttpClient client

  void "test monitor response"() {
    when:
    HttpRequest request = HttpRequest.GET('/monitor')
    String rsp = client.toBlocking().retrieve(request)

    then:
    rsp == '{"status":"RUNNING"}'
  }
}
