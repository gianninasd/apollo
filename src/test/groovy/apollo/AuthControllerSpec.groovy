package apollo

import apollo.domain.CardRequest
import groovy.json.JsonSlurper
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import spock.lang.Specification
import javax.inject.Inject

@MicronautTest
class AuthControllerSpec extends Specification {

    @Inject
    @Client("/")
    RxHttpClient client

    void "test success response"() {
        when:
        CardRequest cardRequest = new CardRequest(merchantRefNum: "test1234", amount: 1500, settleWithAuth: true)
        HttpRequest request = HttpRequest.POST('/auth', cardRequest)
        String rsp = client.toBlocking().retrieve(request)

        then:
        def jsonSlurper = new JsonSlurper()
        def o = jsonSlurper.parseText(rsp)
        o.id != null
        o.status == "COMPLETED"
        o.merchantRefNum == "test1234"
    }

    void "test failed response"() {
        when:
        CardRequest cardRequest = new CardRequest(merchantRefNum: "test1234", amount: 7, settleWithAuth: true)
        HttpRequest request = HttpRequest.POST('/auth', cardRequest)
        client.toBlocking().exchange(request)

        then:
        def ex = thrown(HttpClientResponseException)
        HttpResponse resp = ex.response
        resp.code() == 400
        /*println resp.code()
        println resp.body
        def jsonSlurper = new JsonSlurper()
        def o = jsonSlurper.parseText(resp.body())
        o.id != null
        o.status == "FAILED"
        o.merchantRefNum == "test1234"
        o.error.code == 1007*/
    }
}
