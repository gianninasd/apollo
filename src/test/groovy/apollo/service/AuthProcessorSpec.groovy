package apollo.service

import apollo.domain.CardRequest
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import spock.lang.Specification

@MicronautTest
class AuthProcessorSpec extends Specification {

  AuthProcessor proc = new AuthProcessor()

  void "test return success"() {
    when:
    def uuid = "bla-1234"
    def req = new CardRequest(merchantRefNum: "test1234", amount: 1500)
    def resp = proc.process(uuid, req)

    then:
    resp.httpStatusCode == 200
    resp.authResponse.status == "COMPLETED"
    resp.authResponse.merchantRefNum == "test1234"
    resp.authResponse.authCode != null
  }

  void "test return error 1007"() {
    when:
    def uuid = "bla-1234"
    def req = new CardRequest(merchantRefNum: "test1234", amount: 7)
    def resp = proc.process(uuid, req)

    then:
    resp.httpStatusCode == 400
    resp.authResponse.status == "FAILED"
    resp.authResponse.merchantRefNum == "test1234"
    resp.authResponse.error.code == 1007
  }

  void "test return error 1005"() {
    when:
    def uuid = "bla-1234"
    def req = new CardRequest(merchantRefNum: "test1234", amount: 5)
    def resp = proc.process(uuid, req)

    then:
    resp.httpStatusCode == 400
    resp.authResponse.status == "FAILED"
    resp.authResponse.merchantRefNum == "test1234"
    resp.authResponse.error.code == 1005
  }
}
