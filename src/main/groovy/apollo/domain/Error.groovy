package apollo.domain

class Error {
  int code
  String message

  @Override
  String toString() {
    return "Error{code=${code}, message=${message}}"
  }
}
