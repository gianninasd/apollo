package apollo.domain

/**
 * Contains the status of the service
 */
class Monitor {

  public status = "RUNNING"
}
