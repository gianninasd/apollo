package apollo.domain

class CardRequest {

  String merchantRefNum
  int amount
  boolean settleWithAuth

  @Override
  String toString() {
    return "CardRequest{merchantRefNum=${merchantRefNum}, amount=${amount}}"
  }
}
