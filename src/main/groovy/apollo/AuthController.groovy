package apollo

import apollo.domain.AuthResponse
import apollo.domain.CardRequest
import apollo.domain.CardResponse
import apollo.service.AuthProcessor
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.MutableHttpResponse
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Controller("/auth")
class AuthController {

  private static final Logger log = LoggerFactory.getLogger(AuthController.class);
  private AuthProcessor proc = new AuthProcessor()

  @Post(consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
  MutableHttpResponse<AuthResponse> process(HttpRequest req, @Body CardRequest cardRequest ) {
    log.info("Incoming request from ${req.remoteAddress}")

    // generate UUID
    String uuid = UUID.randomUUID().toString();

    log.info("${uuid} Processing ${cardRequest}")

    // TODO validate request
    // run thru simulator
    CardResponse resp = proc.process(uuid, cardRequest)
    log.info("${uuid} Response was ${resp.authResponse}")

    if( resp.httpStatusCode == 200 )
      HttpResponse.ok().body(resp.authResponse);
    else
      HttpResponse.badRequest(resp.authResponse)
  }
}
