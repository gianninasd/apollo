package apollo.service

import apollo.domain.AuthResponse
import apollo.domain.CardRequest
import apollo.domain.CardResponse

import java.text.SimpleDateFormat

/**
 * Simulator of auth requests
 */
class AuthProcessor {

  private static def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

  /**
   * Process generate either a successful or error response based on the amount
   * @param cardRequest
   * @return
   */
  CardResponse process(String uuid, CardRequest req) {
    CardResponse response
    AuthResponse rawResponse

    switch (req.amount) {
      case 5:
        rawResponse = generateError(uuid, req, 1005, "Transaction declined by the bank")
        response = new CardResponse(httpStatusCode: 400, authResponse: rawResponse)
        break
      case 7:
        rawResponse = generateError(uuid, req, 1007, "Insufficient funds")
        response = new CardResponse(httpStatusCode: 400, authResponse: rawResponse)
        break
      default:
        rawResponse = generateSuccess(uuid, req)
        response = new CardResponse(httpStatusCode: 200, authResponse: rawResponse)
    }

    return response
  }

  /**
   * Generates an error response object
   */
  private static AuthResponse generateError(String uuid, CardRequest req, int code, String mess) {
    def cardResponse = new AuthResponse(
      id: uuid,
      status: "FAILED",
      txnTime: sdf.format(new Date()),
      merchantRefNum: req.merchantRefNum,
      amount: req.amount,
      settleWithAuth: req.settleWithAuth,
      error: new apollo.domain.Error(code: code, message: mess)
    )

    return cardResponse
  }

  /**
   * Generates a successful response object
   */
  private static AuthResponse generateSuccess(String uuid, CardRequest req) {
    def cardResponse = new AuthResponse(
        id: uuid,
        status: "COMPLETED",
        txnTime: sdf.format(new Date()),
        merchantRefNum: req.merchantRefNum,
        amount: req.amount,
        settleWithAuth: req.settleWithAuth,
        authCode: "W08GG1"
    )

    return cardResponse
  }
}
