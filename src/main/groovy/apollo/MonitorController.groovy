package apollo

import apollo.domain.Monitor
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Controller("/monitor")
class MonitorController {

  private static final Logger log = LoggerFactory.getLogger(MonitorController.class);

  @Get(produces = MediaType.TEXT_JSON)
  Monitor monitor() {
    def monitor = new Monitor()
    log.info("Monitor status = ${monitor.status}")
    monitor
  }
}
