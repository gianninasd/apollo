Apollo
================
Sample API simulator written in Groovy with the Micronaut framework.

## Pre-requisites
* Install SDKMan
* Via sdkman, install JDK 11
* Via sdkman, install Groovy 3.0.x
* Via sdkman, install Gradle 6.6.x
* Via sdkman, install Micronaut 2.1.x

## Getting started
* Clone the repo from github to your local machine
* From a terminal run `gradle run`
* Open a browser and navigate to http://localhost:3000/auth

## Automated Tests
* To execute all the tests, from a console run: `gradle test`

## References
* https://sdkman.io/
* https://micronaut.io/
* https://guides.micronaut.io/creating-your-first-micronaut-app-groovy/guide/index.html
* https://docs.gitlab.com/ee/ci/yaml/README.html
* https://hub.docker.com/_/gradle/?tab=description&page=1&name=6.3